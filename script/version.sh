# CI_COMMIT_REF_NAME="main"
# GitVersion_Major=1
# GitVersion_Minor=7
# GitVersion_Patch=4
# GitVersion_MajorMinorPatch="${GitVersion_Major}.${GitVersion_Minor}.${GitVersion_Patch}"

# initialize variables
DEVELOPMENT_BRANCH="main"
VERSION=$GitVersion_MajorMinorPatch
RELEASE=$VERSION
TAG_EXISTS=$(git tag | grep -q "$VERSION" && echo "yes" || echo "no")
RELEASE_NOTES="No stories available";

PREVIOUS_RELEASE_MINOR=$((GitVersion_Minor - 1))

# release branch
if [[ "$CI_COMMIT_REF_NAME" =~ ^release/* && "$TAG_EXISTS" == "yes" ]]; then
  echo "Release branch and tag exists"
  RELEASE="${GitVersion_Major}.${GitVersion_Minor}-rc"
fi

# main branch
if [[ "$CI_COMMIT_REF_NAME" == "$DEVELOPMENT_BRANCH" ]]; then
  RELEASE_TAG="${GitVersion_Major}.${GitVersion_Minor}-rc"
  RELEASE_TAG_EXISTS=$(git tag | grep -q "$RELEASE_TAG" && echo "yes" || echo "no")
  # check if minor version should be increased (new release started)
  if [[ "$RELEASE_TAG_EXISTS" == "yes" ]]; then
    Next_GitVersion_Minor=$((GitVersion_Minor + 1))
    VERSION="${GitVersion_Major}.${Next_GitVersion_Minor}.0"
    RELEASE=$VERSION
    PREVIOUS_RELEASE_MINOR=$GitVersion_Minor
  fi
fi

# collect stories
# get latest release candidate tag
RELEASE_START=$(git tag | grep -E "^[0-9]+\.$PREVIOUS_RELEASE_MINOR-rc$" | sort -V | tail -n 1)
if [[ "$RELEASE_START" != "" ]]; then
  echo "Release start: $RELEASE_START"
  STORIES=$(git log "${RELEASE_START}"..HEAD --pretty=format:"- %s" | grep -E "[feat|fix]" | grep -oE "\[[A-Za-z]+-[0-9]+\]" | sort | uniq)
  if [ -z "$STORIES" ]; then
    RELEASE_NOTES="No stories available";
  else
    RELEASE_NOTES=$(echo "${STORIES}" | tr '\n' ' ');
  fi
fi

echo "Release $RELEASE"
echo "New version $VERSION"
echo "Stories: $RELEASE_NOTES"

echo "RELEASE=$RELEASE" >> variables.env
echo "VERSION=$VERSION" >> variables.env
echo "RELEASE_NOTES=$RELEASE_NOTES" >> variables.env